<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::get('history', 'HistoryController@show');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);





// Route::post('foo/bar', function () {
//     return 'Hello World';
// });
//
// Route::put('foo/bar', function () {
//     return 'Hello World : Put foo/bar';
// });
//
// Route::delete('foo/bar', function () {
//     return 'Hello World : Delete foo/bar';
// });
